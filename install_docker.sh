#!/usr/bin/env bash

curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh

curl -o installDCCommand.sh "https://gitlab.com/mredig/vm-init-scripts/-/raw/main/installDCCommand.sh"
chmod +x installDCCommand.sh
./installDCCommand.sh
source ~/.bashrc