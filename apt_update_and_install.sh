#!/usr/bin/env bash

export DEBIAN_FRONTEND=noninteractive
export DEBCONF_NONINTERACTIVE_SEEN=true

apt-get update
apt-get upgrade -y

apt-get install -y $@

apt-get autoremove -y
