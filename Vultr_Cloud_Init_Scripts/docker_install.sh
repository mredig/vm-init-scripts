#!/usr/bin/env bash

echo "+++=========SSH SETUP===========+++"
mkdir -p /root/.ssh
chmod 700 /root/.ssh
curl https://github.com/mredig.keys > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys

echo "+++=========HOME ROOT SETUP===========+++"

su root
cd /root
export HOME=/root

echo "+++=========UPDATES SETUP===========+++"
export DEBIAN_FRONTEND=noninteractive 
apt-get update
apt-get upgrade -y

echo "+++=========GIT SETUP===========+++"
apt-get install -y git

echo "+++=========DOCKER SETUP===========+++"
curl -fsSL get.docker.com -o get-docker.sh && sh get-docker.sh

echo "+++=========ENV SETUP===========+++"
curl -o installDCCommand.sh "https://gitlab.com/mredig/wan-party-dedicated-servers/-/raw/f790c6df3e6b6c6fa938b41c78c4722b5365b975/installDCCommand.sh?inline=false"
chmod +x installDCCommand.sh
./installDCCommand.sh
source ~/.bashrc

echo "+++=========DEDICATED SERVER COMPOSE SETUP===========+++"

# docker compose up -d 


echo "+++=========FIX SSH===========+++"
cd /etc/ssh

sed 's/#ListenAddress 0/ListenAddress 0/g' sshd_config > sshd_config.2
mv sshd_config.2 sshd_config

echo "+++=========CLEANUP===========+++"
apt-get auto remove -y
reboot # usually some updates require an initial reboot

