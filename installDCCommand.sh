#!/usr/bin/env sh

echo "alias dc='docker compose'\n
alias ga='git add'\n
alias gc='git commit -m'\n
alias gp='git push'\n
alias gs='git status'\n
" >> ~/.bashrc

