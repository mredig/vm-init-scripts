#!/usr/bin/env bash

mkdir -p /root/.ssh
chmod 700 /root/.ssh
curl https://github.com/mredig.keys > /root/.ssh/authorized_keys
chmod 600 /root/.ssh/authorized_keys